# Speak-and-Spell

The Speak and Spell assignment is a realization of the use of Linear Predictive coding or the speech data synthesis and the application of the Yule Walker equations in the formulation of the speech data. The programming is a basic implementation of the speak and spell toy, created by the Texas Instruments, that was one of the primitive speech processing equipment.

## Basic Steps 
In this project, a raw speech sample in .wav format is taken as the data and read into Matlab. The voice sample is converted to data and the autocorrelation is found out using the data. Assuming an impulse input the solutions to the all pole system is found out and the transfer function coefficients are obtained. The programs also plots the pole and magnitude curves of the sounds obtained. The basic sound words that are Vowels. 
Next an input is obtained as a impulse function of duration 1 second and passed through the transfer function obtained. The output of the filter is compared with the original voices, and the resemblance is checked. 
The voices are approximately same and of same pitch.

The whole process of the project step wise is given in the code and can be summarized as follows-

*	Read the audio files to get the inputs 
*	Convert the inputs into a row vector for further processing
*	Calculate the autocorrelation of the audio data obtained
*	Form a Yule Walker Equation for solving the function
*	Solve the Yule Walker Equations using the basic method at first and then the fast solution method 
*	Cross Check the answers using the arburg function, to verify if the coefficients obtained are correct or not
*	Now filter the coefficients to form the original equation required
*	Generate an impulse signal with the sampling frequency (taken 8000 Hz) and pitch between 150 – 200 Hz.
*	Pass the input through the filter to generate an output of the system
*	Compare the system’s output with the input voice and check how much similar is the quality of voice.
*	Compare the system’s output with the input voice and check how much similar is the quality of voice.
*	Form the pole- zero representation and the magnitude plot of the transfer function frequency response.

## Function Scripts

* To agenerate auto-correlation 
```
function [r] = auto(a, n)
```

* To solve the Yule-Walker equations (Fast Solve)
```
function [f] = fast_yw(r)
```

* To filter output using transfer function
```
function [g] = my_filt(filtcoff, pulse)
```
## Pole-Zero plots for english vowels
![a](https://github.com/Sidhant-Chatterjee/Speak-and-Spell/blob/master/pole%20plot%20for%20a.JPG)

![e](https://github.com/Sidhant-Chatterjee/Speak-and-Spell/blob/master/pole%20plot%20for%20e.JPG)

![i](https://github.com/Sidhant-Chatterjee/Speak-and-Spell/blob/master/pole%20plot%20for%20i.JPG)

![o](https://github.com/Sidhant-Chatterjee/Speak-and-Spell/blob/master/pole%20plot%20for%20o.JPG)

![u](https://github.com/Sidhant-Chatterjee/Speak-and-Spell/blob/master/pole%20plot%20for%20u%20and%20ah.JPG)

## Magnitude plots 
![a_e_ah](https://github.com/Sidhant-Chatterjee/Speak-and-Spell/blob/master/magnitude%20plot%20a%20e%20ah.JPG)

![i_o_u](https://github.com/Sidhant-Chatterjee/Speak-and-Spell/blob/master/magnitude%20plot%20i%20o%20u.JPG)

## YW Equation and combined response
Application of the Yule – Walker Equations:
The Yule Walker equations help in solving the system of the all pole AR process, so that we can obtain the coefficients of the filter. The coefficients can be obtained by solving a Toeplitz matrix and using the inverse operation to get the coefficients. The solutions of the equation help to define the transfer function of the system.

The Combined Magnitude graph can also be plotted using the ‘fvtool’ function that yields the following output. 

![mag_resp](https://github.com/Sidhant-Chatterjee/Speak-and-Spell/blob/master/whole%20mag%20response.JPG)

## Authors

* **Sidhant Chatterjee** - [Homepage](http://sidhant-chatterjee.net) - For Mathematical Methods for Signal-Processing course - ECE 6010

## Acknowledgments

* [Utah State Electrical and Computer Engineering Department](https://ece.usu.edu/index)
* [Professor Todd Moon](https://ece.usu.edu/people/faculty/moon-todd)
* [Mathematical Methods for Signal Processing - Dr. Todd K. Moon](https://www.pearson.com/us/higher-education/program/Moon-Mathematical-Methods-and-Algorithms-for-Signal-Processing/PGM279076.html)

