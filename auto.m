%***********fUNCTION TO GENERATE AUTOCORELATION******************
function [r] = auto(a, n)
   
   len = length(a);
   raa = conv(a, fliplr(a));
   r = raa(len : len + n);
    %r = r';
end

