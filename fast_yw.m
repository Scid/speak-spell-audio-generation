%*********************Fast Yule Walker Solver****************************
function [f] = fast_yw(r) % To calculate yule walker cofficients
len = length(r) - 1;
len1 = len - 1;             % the length used for the matrix
r = r(:);
r0 = r(1);                    % The first element as r(1)
W  = (r(2)/r(1));
for i = 1:len1
   %rt = r(2:(i+1));
   Jw = fliplr(W);
   % To find the value of alpha, we use the below formula as given by the
   % levinson Durbin Algorithim and includes the conjugate case also
   a = (r(i+2) - (r(2:(i+1)).' * Jw)) / (r0 - r(2:(i+1)).' * conj(W)); 
   b = (W - (conj(Jw) * a ));
   
   f = [ b; a ];     % The output is the ratio of these two.
   %f = [1 (-1*ff.')];
end
end

