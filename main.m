%************************************************************************
%********************SPEAK AND SPELL*************************************
clear all
close all
%*Getting the audio files into data files
[La, Fs] = wavread('longa.wav');  Sa = wavread('shorta.wav');
Le = wavread('longe.wav');  Se = wavread('shorte.wav');
Li = wavread('longi.wav');  Si = wavread('shorti.wav');
Lo = wavread('longo.wav');  So = wavread('shorto.wav');
Lu = wavread('longu.wav');  Su = wavread('shortu.wav');
Ah = wavread('ah.wav');
%*************************************************************************
%*************************************************************************
%************** Converiting into column matrices**************************
la = La'; le = Le'; li = Li'; lo = Lo'; lu = Lu';
sa = Sa'; se = Se'; si = Si'; so = So'; su = Su';
ah = Ah'; 

%************************************************************************
%********* Autocorrelation Conversion (ac)*******************************
n = 13;
la_ac = auto(la, n);   sa_ac = auto(sa, n);
le_ac = auto(le, n);   se_ac = auto(se, n);
li_ac = auto(li, n);   si_ac = auto(si, n);
lo_ac = auto(lo, n);   so_ac = auto(so, n);
lu_ac = auto(lu, n);   su_ac = auto(su, n);
ah_ac = auto(ah, n);

%************************************************************************
%*****************Yule Walker Solver Matlab Basic ***********************

basic_la = yule_basic(la_ac); basic_sa = yule_basic(sa_ac);
basic_le = yule_basic(le_ac); basic_se = yule_basic(se_ac);
basic_li = yule_basic(li_ac); basic_si = yule_basic(si_ac);
basic_lo = yule_basic(lo_ac); basic_so = yule_basic(so_ac);
basic_lu = yule_basic(lu_ac); basic_su = yule_basic(su_ac);
basic_ah = yule_basic(ah_ac);
%********************************************************************
%***********Fast Solution of Yule-Walker Equation********************
fast_la = fast_yw(la_ac);  fast_sa = fast_yw(sa_ac);
fast_le = fast_yw(le_ac);  fast_se = fast_yw(se_ac);
fast_li = fast_yw(li_ac);  fast_si = fast_yw(si_ac);
fast_lo = fast_yw(lo_ac);  fast_so = fast_yw(so_ac);
fast_lu = fast_yw(lu_ac);  fast_su = fast_yw(su_ac);
fast_ah = fast_yw(ah_ac);
%********************************************************************
%*******Comparing with the function arburg **************************
compare_la = arburg(La,n);  compare_sa = arburg(sa,n);
compare_le = arburg(Le,n);  compare_se = arburg(sa,n);
compare_li = arburg(Li,n);  compare_si = arburg(sa,n);
compare_lo = arburg(Lo,n);  compare_so = arburg(sa,n);
compare_lu = arburg(Lu,n);  compare_su = arburg(sa,n);
compare_ah = arburg(Ah,n);
%********************************************************************
%***************Results are aproimately same*************************
%***************Filtering The Cofficients****************************
filtcoff_la = [1, basic_la];  filtcoff_sa = [1, basic_sa];
filtcoff_le = [1, basic_le];  filtcoff_se = [1, basic_se];
filtcoff_li = [1, basic_li];  filtcoff_si = [1, basic_si];
filtcoff_lo = [1, basic_lo];  filtcoff_so = [1, basic_so];
filtcoff_lu = [1, basic_lu];  filtcoff_su = [1, basic_su];
filtcoff_ah = [1, basic_ah];
%********************************************************************
%*****************Creating Impulse Train*****************************
fs = 8000;
p = 180;
interval = ceil(fs/p);
t = 0:(1/fs):1;                     
pulse = zeros(size(t));
pulse(1:interval:end) = 1;
%*********************************************************************
%*********** Filtered output using the pulse responses****************
out_la = my_filt(filtcoff_la, pulse); out_sa = my_filt(filtcoff_sa, pulse);
out_le = my_filt(filtcoff_le, pulse); out_se = my_filt(filtcoff_se, pulse);
out_li = my_filt(filtcoff_li, pulse); out_si = my_filt(filtcoff_si, pulse);
out_lo = my_filt(filtcoff_lo, pulse); out_so = my_filt(filtcoff_so, pulse);
out_lu = my_filt(filtcoff_lu, pulse); out_su = my_filt(filtcoff_su, pulse);
out_ah = my_filt(filtcoff_ah, pulse);
%**********************************************************************
%********************Plotting Pole Locations***************************
figure; zplane(1, filtcoff_la)
title('Pole plot for long a'); figure; zplane(1, filtcoff_le)
title('Pole plot for long e'); figure; zplane(1, filtcoff_li)
title('Pole plot for long i'); figure; zplane(1, filtcoff_lo)
title('Pole plot for long o'); figure; zplane(1, filtcoff_lu)
title('Pole plot for long u'); figure; zplane(1, filtcoff_sa)
title('Pole plot for short a'); figure; zplane(1, filtcoff_se)
title('Pole plot for short e'); figure; zplane(1, filtcoff_si)
title('Pole plot for short i'); figure; zplane(1, filtcoff_so)
title('Pole plot for short o'); figure; zplane(1, filtcoff_su)
title('Pole plot for short u'); figure; zplane(1, filtcoff_ah)
title('Pole plot for ah');

%************************* Magnitude Plot*******************************
 range = linspace(-pi, +pi);
[f_la, w_la] = freqz(1, filtcoff_la, range);
[f_le, w_le] = freqz(1, filtcoff_le, range);
[f_li, w_li] = freqz(1, filtcoff_li, range);
[f_lo, w_lo] = freqz(1, filtcoff_lo, range);
[f_lu, w_lu] = freqz(1, filtcoff_lu, range);
[f_sa, w_sa] = freqz(1, filtcoff_sa, range);
[f_se, w_se] = freqz(1, filtcoff_se, range);
[f_si, w_si] = freqz(1, filtcoff_si, range);
[f_so, w_so] = freqz(1, filtcoff_so, range);
[f_su, w_su] = freqz(1, filtcoff_su, range);
[f_ah, w_ah] = freqz(1, filtcoff_ah, range);
figure;
% plot(w_la, abs(f_la), 'g',w_sa, abs(f_sa), 'y')
% title('Magnitude plot for sound a (with green as long)');figure;
% plot(w_le, abs(f_le), 'g', w_se, abs(f_se), 'y')
% title('Magnitude plot for sound e (with green as long)');figure;
% plot(w_li, abs(f_li), 'g', w_si, abs(f_si), 'y')
% title('Magnitude plot for sound i (with green as long)');figure;
% plot(w_lo, abs(f_lo), 'g', w_so, abs(f_so), 'y')
% title('Magnitude plot for sound o (with green as long)');figure;
% plot(w_lu, abs(f_lu), 'g',w_su, abs(f_su), 'y')
% title('Magnitude plot for sound u (with green as long)');figure;
% plot(w_ah, abs(f_ah), 'k')
% title('Magnitude plot for sound ah ');
%************************************************************************
 fvtool(1, filtcoff_la, 1, filtcoff_sa, 1,filtcoff_le, 1, filtcoff_se, 1, filtcoff_li, 1, filtcoff_si, 1, filtcoff_lo, 1, filtcoff_so, 1,filtcoff_lu, 1, filtcoff_su, 1, filtcoff_ah) 
%*****************End_Speak_and_Spell************************************
figure();
plot(pulse, out_la, 'r');

