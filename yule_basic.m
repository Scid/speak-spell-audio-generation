%********************YW equation solver_Basic Method******************
function [w] = yule_basic(r)
R = toeplitz(r(1: end-1));
s = r(2: end)';
w = R\s;
w = -w';
end
